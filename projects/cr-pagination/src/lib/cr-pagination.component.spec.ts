import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrPaginationComponent } from './cr-pagination.component';

describe('CrPaginationComponent', () => {
  let component: CrPaginationComponent;
  let fixture: ComponentFixture<CrPaginationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrPaginationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
