import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'cr-pagination',
  templateUrl: './cr-pagination.component.html',
  styleUrls: ['./cr-pagination.component.scss']
})
export class CrPaginationComponent implements OnInit, OnChanges {

  @Input() totalItems = 0;
  @Input() currentPage = 0;
  @Input() perPage = 10;
  @Input() pageOptions = [
    {
      label: 10,
      value: 10
    },
    {
      label: 30,
      value: 30
    },
    {
      label: 50,
      value: 50
    },
    {
      label: 100,
      value: 100
    },
    {
      label: 250,
      value: 250
    },
  ];
  @Output() changePage = new EventEmitter();
  totalPages = 0;
  startItem = 1;
  endItem = 1;
  totalPageList = [];

  constructor() { }

  ngOnInit() {
    this.itemsPerPage();
  }

  ngOnChanges(changes: any) {
    if (changes.totalItems && !changes.totalItems.firstChange && !changes.currentPage) {
      this.itemsPerPage();
    }
    if (changes.perPage && !changes.perPage.firstChange && !changes.currentPage) {
      this.itemsPerPage();
    }
    if (changes.currentPage && !changes.currentPage.firstChange) {
      // this.itemsPerPage();
    }
  }

  goToPage(i) {
    if (i >= 0 && i < this.totalPages && this.currentPage !== i) {
      this.currentPage = i;
      this.changePagination();
      // this.changePage.emit({ currentPage: this.currentPage, perPage: this.perPage });
    }
  }

  itemsPerPage() {
    this.totalPageList = [];
    this.currentPage = 0;
    this.startItem = 1;
    if (this.totalItems > this.perPage) {
      this.endItem = this.perPage;
      this.totalPages = Math.ceil(this.totalItems / this.perPage);
    } else {
      this.endItem = this.totalItems;
      this.totalPages = 1;
    }
    for (let i = 1; i <= this.totalPages; i++) {
      this.totalPageList.push(i);
    }
    this.changePage.emit({ currentPage: this.currentPage, perPage: this.perPage });
  }

  changePagination() {
    if (this.currentPage > 0) {
      this.startItem = this.currentPage * this.perPage + 1;
    } else {
      this.startItem = this.currentPage + 1;
    }

    if (this.currentPage === this.totalPages - 1) {
      this.endItem = this.totalItems;
    } else {
      this.endItem = (this.currentPage + 1) * this.perPage;
    }
    this.changePage.emit({ currentPage: this.currentPage, perPage: this.perPage });
  }

  requiredBefore() {
    if (this.currentPage > 3) {
      return true;
    } else {
      return false;
    }
  }

  requiredAfter() {
    if (this.currentPage < (this.totalPages - 4)) {
      return true;
    } else {
      return false;
    }
  }
}
