import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cr-pagination-sandbox';
  params = {
    page: 0,
    perPage: 10
  };

  changePage(event) {
    this.params.page = event.currentPage;
    this.params.perPage = event.perPage;
  }
}
